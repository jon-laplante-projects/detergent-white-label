// Node packages.
var open = require("open");
var gui = require('nw.gui');

// Enable copy/paste on OS X.
// initialize window menu
var win = gui.Window.get(),
    nativeMenuBar = new gui.Menu({
        type: "menubar"
    });

// check operating system for the menu
if (process.platform === "darwin") {
    nativeMenuBar.createMacBuiltin("Your App Name");
}

// actually assign menu to window
win.menu = nativeMenuBar;
// End OS X copy/paste code.

// Object literal.
var Quicklist = Quicklist || {};

Quicklist.App = {
  fromCol: null,
  filterCol: null,
  fromArray: [],
  filterArray: [],
  fromColArray: [],
  filterColArray: [],
  fileArray: []
};

function process_file(){
  var filter = Quicklist.App.filterArray;
  var source = Quicklist.App.fromArray;

  $.each(filter, function(key, val){
    //if (key > 0) {
      if (val[Quicklist.App.filterCol] != null && val[Quicklist.App.filterCol] != "") {
        Quicklist.App.filterColArray.push( val[Quicklist.App.filterCol].toLowerCase() );
      }
    //}

    // Remove the duplicates from this array.
    Quicklist.App.filterColArray = $.unique(Quicklist.App.filterColArray);
  });

  $.each(source, function(key, val){
    //if (key > 0) {
        if ( val[Quicklist.App.fromCol] != null && val[Quicklist.App.fromCol] != "" ) {
            Quicklist.App.fromColArray.push( val[Quicklist.App.fromCol].toLowerCase() );
        }
    //}

    // Remove the duplicates from this array.
    Quicklist.App.fromColArray = $.unique(Quicklist.App.fromColArray);
  });

  // Compare the arrays and remove duplicates.
  $.each(Quicklist.App.filterColArray, function(i, val){
    if($.inArray(val.toLowerCase(), Quicklist.App.fromColArray) < 1)
        Quicklist.App.fileArray.push(val.toLowerCase());
  });

  //Quicklist.App.fileArray.unshift( Quicklist.App.filterArray[0][Quicklist.App.filterCol] );

  // download stuff
  var fileName = "filtered_data.csv";
  var buffer = Quicklist.App.fileArray.join("\n");
  var blob = new Blob([buffer], {
    "type": "text/csv;charset=utf8;"
  });
  var link = document.createElement("a");

  if(link.download !== undefined) { // feature detection
    // Browsers that support HTML5 download attribute
    link.setAttribute("href", window.URL.createObjectURL(blob));
    link.setAttribute("download", fileName);
   }
  else {
    // it needs to implement server side export
    link.setAttribute("href", "http://www.example.com/export");
  }
  document.body.appendChild(link);

  link.click();

  // Hide the notification alert.
  $("#notifications_div").hide();
}

$(function(){
  // Ensure we don't lose the background on the body.
  $("body").addClass("fs-body");

  // If the user has opted out 10 times or has subscribed, don't show the subscribe dialog.
  if (localStorage.opt_outs > 9 || localStorage.email_captured === "true") {
    $("#sign_up_pnl").hide();
  }

  // Check to see if File APIs are supported.
  if (!window.File || !window.FileReader || !window.FileList || !window.Blob) {
    $("#file_unsupported_alert").css("display", "block");
    return;
  }

  $("#from_list").fileinput({
    allowedFileExtensions: ["csv","txt"]
  });

  $("#filter_list").fileinput({
    allowedFileExtensions: ["csv","txt"]
  });


  // Initialize the carousel for the banner rotator.
  $('.carousel').carousel({
    interval: 10000
  });

  // Open banner links in a browser.
  $(".banner-link").click(function(){
    var link = $(this).attr("data-src");

    open(link);
  });

  // Open links in default browser.
  $("a[href]").click(function(e){
    e.preventDefault();

    var link = $(this).attr("href");

    open(link);
  });

  // If a form is submitted, intercept it.
  $("body").on('click', 'input[type="submit"]', function(event){
    //event.stopPropagation();
    //event.preventDefault();
    localStorage.email_captured = true;

    $("#close_subscribe_btn").text("Close");

    //console.log("Form submission stopped.");
  });

  // // If a button is clicked, check if it is a submit.
  // $("button").not("[type='reset']").not("[type='button']").click(function(event){
  //   //event.stopPropagation();
  //   //event.preventDefault();
  //   localStorage.email_captured = true;
  //
  //   $("#close_subscribe_btn").text("Close");
  //
  //   //console.log("Form submission stopped.");
  // });

  // If a user doesn't sign up and closes the window instead, make note of it.
  $("#close_subscribe_btn").click(function(){
    if (parseInt(localStorage.opt_outs) === null) {
      localStorage.opt_outs = 0;
      console.log("opt outs == null");
    } else {
      console.log("opts outs == int");
      localStorage.opt_outs = parseInt(localStorage.opt_outs) + 1;
    }

    $("#sign_up_pnl").hide();
  });

  // Check to see if a file is present.
  $("#from_list").change(function(){
    if ($("#from_list").val() != null && $("#from_list").val() != "") {
      $("#from_list").parse({
        config: {
          worker: true,
          step: true,
          complete: function(results, file) {
            Papa.parse(from_list.files[0], {
            complete: function(results) {
                Quicklist.App.fromArray = results["data"];

                // Clear the radio HTML section.
                $("#column_radios").html("");

            		$.each(results["data"][0], function(key, val){
                  if (val != "" && val != null) {
                    $("#column_radios").append('<p><input type="radio" name="columns" value="' + key + '" />&nbsp;&nbsp;' + val + '</p>');
                  }
                });

                $("#radio_cols_panel").slideDown("fast");
            	}
            });
          }
        }
      });

      // If both files are uploaded, make the download button active.
      if ($("#filter_list").val() != null && $("#filter_list").val() != "") {
        $("#download_btn").attr("disabled", false);
      } else {
        $("#download_btn").attr("disabled", "disabled");
      }
    } else {
      $("#radio_cols_panel").slideUp("fast");

      // If either file is missing, disable the "Download" button.
      $("#download_btn").attr("disabled", "disabled");
    }
  });

  $("#filter_list").change(function(){
    if ($("#filter_list").val() != null && $("#filter_list").val() != "") {
      $("#filter_list").parse({
        config: {
          worker: true,
          step: true,
          complete: function(results, file) {
            Papa.parse(filter_list.files[0], {
              complete: function(results) {
            		Quicklist.App.filterArray = results["data"];

                // Clear the radio HTML section.
                $("#filter_radios").html("");

                $.each(results["data"][0], function(key, val){
                  if (val != "" && val != null) {
                    $("#filter_radios").append('<p><input type="radio" name="filter_cols" value="' + key + '" />&nbsp;&nbsp;' + val + '</p>');
                  }
                });

                $("#filter_cols_panel").slideDown("fast");
            	}
            });
          }
        }
      });
    } else {
      $("#filter_cols_panel").slideUp("fast");

      // If either file is missing, disable the "Download" button.
      $("#download_btn").attr("disabled", "disabled");
    }
  });

  // When a file is removed from the input, clear and hide the column lists.
  $(".fileinput-remove").click(function(){
    $(this).parent().parent().parent().next().slideUp("fast");
  });

  // Check what column is selected.
  $("body").on("change", 'input[name="columns"]', function(){
    Quicklist.App.fromCol = $('input[name="columns"]:checked').val();
    //$('input[name="columns"]:checked').val();

    // If both files are uploaded, make the download button active.
    if ( !$('input[name="filter_cols"]:checked').val() ) {
      $("#download_btn").attr("disabled", "disabled");
    } else {
      $("#download_btn").attr("disabled", false);
    }
  });

  // Check what column is selected.
  $("body").on("change", 'input[name="filter_cols"]', function(){
    Quicklist.App.filterCol = $('input[name="filter_cols"]:checked').val();

    // If both files are uploaded, make the download button active.
    if ( !$('input[name="columns"]:checked').val() ) {
      $("#download_btn").attr("disabled", "disabled");
    } else {
      $("#download_btn").attr("disabled", false);
    }
  });

  // generate and download the file.
  $("#download_btn").click(function(){
    // Show download processing message.
    $("#notifications_div").show();

    // Show download processing system notification.
    var options = {
      icon: "/assets/images/logo.gif",
      body: "Creating your .csv file. For large datasets, this may take awhile."
    };

    var notification = new Notification("Processing selected results.",options);
    notification.onclick = function () {
      document.getElementById("output").innerHTML += "Notification clicked";
    }

    notification.onshow = function () {
      // auto close after 1 second
      setTimeout(function() {notification.close();}, 2000);

      // Generate and download the file.
      process_file();
    }
  });

  // Show the "About Panel".
  $("#about_btn").click(function(){
    $("#about_panel").slideDown("fast");
  });

  // Close the "About Panel".
  $("#close_about_btn").click(function(){
    $("#about_panel").slideUp("fast");
  });

  // Show the "Help Panel".
  $("#help_btn").click(function(){
    $("#help_panel").slideDown("fast");
  });

  // Hide the "Help Panel".
  $("#close_help_btn").click(function(){
    $("#help_panel").slideUp("fast");
  });

  // Refresh button.
  $("#refresh_btn").click(function(){
    location.reload();
  });

  // Open link to Klickrr in browser.
  $("#klickrr_link").click(function(){
    open("http://www.klickrr.net");
  });

  // Open link to Background credits in browser.
  $("#bg_link").click(function(){
    open("https://www.flickr.com/photos/gabrielsaldana/7488905640");
  });

  // Mailto Jon in browser.
  $("#jlaplante_link").click(function(){
    open("mailto:jon.laplante@jonlaplante.com");
  });
});
