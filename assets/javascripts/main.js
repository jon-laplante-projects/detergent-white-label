// Node packages.
fs = require('fs-extra');
path = require('path');
ncp = require('ncp');
child_process = require('child_process');
validUrl = require('valid-url');
var open = require("open");
//nw_init = require("nw-init");
//grunt_cli = require("grunt-cli");

// Classes.
NwBuilder = require('nw-builder');

// Additional initializers.

// Load native UI library
var gui = require('nw.gui');
var win = gui.Window.get();

// Create an empty menu
var menu = new gui.Menu();

// Popup as context menu
menu.popup(10, 10);

var nativeMenuBar = new gui.Menu({ type: "menubar" });
nativeMenuBar.createMacBuiltin("Klickrr List Cleaner");
win.menu = nativeMenuBar;

// We can not create a clipboard, we have to receive the system clipboard
var clipboard = gui.Clipboard.get();

// initialize window menu
var win = gui.Window.get(),
    nativeMenuBar = new gui.Menu({
        type: "menubar"
    });

// check operating system for the menu
if (process.platform === "darwin") {
    nativeMenuBar.createMacBuiltin("Your App Name");
}

// actually assign menu to window
win.menu = nativeMenuBar;

var editor_dom = $.summernote.core.dom;

// Application variables.
background_image_list = ["assets/images/backgrounds/airplane_husk.jpg","assets/images/backgrounds/american_sparklers.jpg","assets/images/backgrounds/beach_at_sunset.jpg","assets/images/backgrounds/calendar_planner.jpg","assets/images/backgrounds/charming_city_street.jpg","assets/images/backgrounds/cherry_blossom_alley.jpg","assets/images/backgrounds/city_skyline.jpg","assets/images/backgrounds/crowd_watching.jpg","assets/images/backgrounds/fireworks.jpg","assets/images/backgrounds/lighthouse.jpg","assets/images/backgrounds/lone_tree_on_snow.jpg","assets/images/backgrounds/pine_tree_in_forest.jpg","assets/images/backgrounds/snowy_mountain.jpg","assets/images/backgrounds/snowy_mountain_village_at_night.jpg","assets/images/backgrounds/swimming_seal.jpg"];
background_thumbs_list = ["assets/images/backgrounds/thumbnails/airplane_husk_thumb.jpg","assets/images/backgrounds/thumbnails/american_sparklers_thumb.jpg","assets/images/backgrounds/thumbnails/beach_at_sunset_thumb.jpg","assets/images/backgrounds/thumbnails/calendar_planner_thumb.jpg","assets/images/backgrounds/thumbnails/charming_city_street_thumb.jpg","assets/images/backgrounds/thumbnails/cherry_blossom_alley_thumb.jpg","assets/images/backgrounds/thumbnails/city_skyline_thumb.jpg","assets/images/backgrounds/thumbnails/crowd_watching_thumb.jpg","assets/images/backgrounds/thumbnails/fireworks_thumb.jpg","assets/images/backgrounds/thumbnails/lighthouse_thumb.jpg","assets/images/backgrounds/thumbnails/lone_tree_on_snow_thumb.jpg","assets/images/backgrounds/thumbnails/pine_tree_in_forest_thumb.jpg","assets/images/backgrounds/thumbnails/snowy_mountain_thumb.jpg","assets/images/backgrounds/thumbnails/snowy_mountain_village_at_night_thumb.jpg","assets/images/backgrounds/thumbnails/swimming_seal_thumb.jpg"];

// Build variables.
var title = "Detergent List Cleaner";
var logo_img;
var tag = "List Cleaner";
var app_name = "Klickrr List Filter";
var background_img;
var include_sign_up = false;
var sign_up_form_code;
var include_about_us = false;
var about_us_html;
var include_banners = false;
var banner_arr = [];
var banners = "";

// Instantiate the node-webkit-builder.
nw = new NwBuilder({
  files: ['./build/**/**'],
  platforms: ['osx32','win32'],
  appName: title,
  buildDir: './../release'
});

// Remove elements from string.
$.strRemove = function(theTarget, theString) {
  return $("<div/>").append(
    $(theTarget, theString).remove().end()
  ).html();
};

function remove_build_dir(){
  try {
    // Remove the generated and edited "build" folder.
    child_process.execSync('rm -r build', function (error, stdout, stderr) {
      if(error === null) {
        console.log(error.code);
      } else {
        console.log("Successfully removed build directory.");
      }
    });
  } catch (e) {
    console.log(e);
  }

  $(".busy-cleaning").removeClass("busy-active");
}

function inject_image(image_src){
  console.log("Adding images to app build folder.");
  var base_image = "";

  base_image = path.parse(image_src);

  // Copy the logo image.
  fs.copy(image_src, 'build/assets/images/' + base_image.base, function (err, data) {
    if (err) {
      console.log("Error copying image: " + err);
    } else {
      console.log("Successfully copied image.");
    }
  });
}

function inject_logo_image(){
  var logo_image = $("img", "#logo_preview").attr("src");
  var base_logo = "";

  base_logo = path.parse(logo_image);

  // Copy the logo image.
  fs.copy(path.resolve(logo_image), 'build/assets/images/' + base_logo.base, function (err, data) {
    if (err) {
      console.log("Error copying bg image: " + err);
    } else {
      console.log("Successfully copied logo image.");
    }
  });
}

function inject_background_image(){
  var bg_image = $("img", "#bg_preview").attr("src");
  //var bg_image = $("#bg_preview").val();
  var base_img = "";

  base_img = path.parse(bg_image);

  // Copy the bg image to the build folder.
  fs.copy(path.resolve(bg_image), 'build/assets/images/' + base_img.base, function (err, data) {
    if (err) {
      console.log("Error copying bg image: " + err);

      fs.exists('/build/assets/images/' + base_img.base, function(exists) {
        if (exists) {
          // Change indicator.
          $(".busy-downloading").removeClass("busy-active");
          $(".busy-building").addClass("busy-active");

          // Build the apps.
          child_process.execSync('grunt app:[osx32,win32] package', function(error, stdout, stderr){
            if (!error) {
              $(".busy-building").removeClass("busy-active");
              $(".busy-cleaning").addClass("busy-active");
               console.log('all done!');

              // Change the icons so user knows something is happening.
              setTimeout(function(){
                $("#building_app_pnl").hide();
                $("#completed_pnl").show();
                // $("#main_app_pnl").show();
              }, 1500);

               remove_build_dir();
            } else {
              console.log(error.code);
              console.log("Building error: " + error);

              remove_build_dir();
            }
          });

          // // Build the apps.
          // nw.build().then(function() {
          //   $(".busy-building").removeClass("busy-active");
          //   $(".busy-cleaning").addClass("busy-active");
          //    console.log('all done!');
          //
          //   // Change the icons so user knows something is happening.
          //   setTimeout(function(){
          //     $("#building_app_pnl").hide();
          //     $("#completed_pnl").show();
          //     // $("#main_app_pnl").show();
          //   }, 1500);
          //
          //    remove_build_dir();
          // }).catch(function (error) {
          //     console.log("WARNING! Building your app has failed.");
          //     console.log("Building error: " + error);
          //
          //     remove_build_dir();
          // });
        }
      });
    } else {
      console.log("Success copying backgound image.");

      // Build the apps.
      child_process.execSync('grunt app package', function(error, stdout, stderr){
        if (!error) {
          console.log('Your app has been built!');

          $(".busy-building").removeClass("busy-active");
          $(".busy-cleaning").addClass("busy-active");
           console.log('all done!');

          // Change the icons so user knows something is happening.
          setTimeout(function(){
            $("#building_app_pnl").hide();
            $("#completed_pnl").show();
            // $("#main_app_pnl").show();
          }, 1500);

           remove_build_dir();
        } else {
          console.log(error.code);
          console.log("Building error: " + error);

          remove_build_dir();
        }
      });

      // // Build the apps.
      // nw.build().then(function() {
      //    console.log('Your app has been built!');
      //
      //    // Keep icons shown so user knows something is happening.
      //    setTimeout(function(){
      //      $(".busy-building").removeClass("busy-active");
      //      $(".busy-cleaning").addClass("busy-active");
      //
      //      $("#building_app_pnl").hide();
      //      $("#completed_pnl").show();
      //      // $("#main_app_pnl").show();
      //    }, 1500);
      //
      //    remove_build_dir();
      // }).catch(function (error) {
      //     console.log("Building failed.");
      //     console.log("Building error: " + error);
      //
      //     remove_build_dir();
      // });
    };
  });
}

// Add items to banners.
function add_banner_items(){
  var indicators = "";
  var items = "";

  // Get the images.
  $.each($(".banner-img-file"), function(key, val){
    if ( $(val).val() !== null ) {
      // Put the image in the 'build' folder.
      inject_image( $(val).val() );

      // Build the banner strings.
      var img = path.parse( $(val).val() );
      var banner_class = "banner-" + parseInt(key + 1);
      if (key > 0) {
        items += '<div class="banner-' + key + ' item banner-link" data-src="' + $(".banner-links").eq(key).val() + '"><img id="banner_img_' + key + '" src="assets/images/' + img.base + '" alt="" /></div>';
      } else {
        items += '<div class="banner-' + key + ' item banner-link active"  data-src="' + $(".banner-links").eq(key).val() + '"active"><img id="banner_img_' + key + '" src="assets/images/' + img.base + '" alt="" /></div>';
      }
    }
  });

  banners = $("#banner_slider_tmpl").html();

  //banners = banners.replace(/{{banner_indicators}}/g, indicators);
  banners = banners.replace(/{{banner_items}}/g, items);

  return banners;
}

// Toggle 'Build' button enabled/disabled.
function buildable_check(){
  console.log("Build check...");
  var empty_required = $('.required').filter(function(){
    return $.trim(this.value).length === 0;
  }).length > 0;

  if (empty_required === true) {
    $("#build_my_app_btn").attr("disabled", "disabled");
  } else {
    $("#build_my_app_btn").attr("disabled", false);
  }

  // If some switches are on, make sure there are values, too.
  if ( include_about_us === true && $("#about_us_html").code() === null ) {
    $("#build_my_app_btn").attr("disabled", "disabled");
  }

  if ( include_sign_up === true && $("#sign_up_code").val() === null ) {
    $("#build_my_app_btn").attr("disabled", "disabled");
  }

  if ( include_banners === true ) {
    var empty_banners = 0;
    $.each($(".banner-img-file"), function(key, val){
      if ($(val).val() === "") {
        empty_banners = parseInt(empty_banners) + 1;
      }
    });

    if (empty_banners === 3) {
      $("#build_my_app_btn").attr("disabled", "disabled");
    } else {
      // If there is a banner image, it needs a link.
      if ( $("#banner_img_1").val() !== "" && $("#banner_link_1").val() === "" ) {
        $("#build_my_app_btn").attr("disabled", "disabled");
      }

      if ( $("#banner_img_2").val() !== "" && $("#banner_link_2").val() === "" ) {
        $("#build_my_app_btn").attr("disabled", "disabled");
      }

      if ( $("#banner_img_3").val() !== "" && $("#banner_link_3").val() === "" ) {
        $("#build_my_app_btn").attr("disabled", "disabled");
      }

      // If there is a banner link, it needs an image.
      if ( $("#banner_img_1").val() === "" && $("#banner_link_1").val() !== "" ) {
        $("#build_my_app_btn").attr("disabled", "disabled");
      }

      if ( $("#banner_img_2").val() === "" && $("#banner_link_2").val() !== "" ) {
        $("#build_my_app_btn").attr("disabled", "disabled");
      }

      if ( $("#banner_img_3").val() === "" && $("#banner_link_3").val() !== "" ) {
        $("#build_my_app_btn").attr("disabled", "disabled");
      }
    }
  }

  // If there is no selected background image, disable the button.
  if ( $("#bg_preview").html() === "" ) {
    $("#build_my_app_btn").attr("disabled", "disabled");
  }
}

// Install makensis so we can roll Windows installers.
function install_makensis() {
  child_process.execSync('brew install makensis', function(error, stdout, stderr){
    if(error === null) {
      console.log(error.code);
    } else {
      console.log("Successfully installed Makensis.");
    }
  });
}

// Install wine so we can roll Windows installers.
function install_wine(){
  child_process.execSync('brew install wine', function(error, stdout, stderr){
    if(error === null) {
      console.log(error.code);
    } else {
      console.log("Successfully installed Wine.");
    }
  });
}

$(function(){
  // Clear out the build folder if it exists.
  remove_build_dir();

  // Initialize help popovers.
  $('[data-toggle="popover"]').popover({
    template: '<div class="popover light-text" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>',
    html: true
  });

  // Initialize "About Us" toggle.
  $("#include_about_us").bootstrapSwitch();

  // Initialize "Sign up" toggle.
  $("#include_sign_up").bootstrapSwitch();

  // Initialize "banner" toggle.
  $("#include_banners").bootstrapSwitch();

  //
  $('input[id="include_about_us"]').on('switchChange.bootstrapSwitch', function(event, state) {
    $("#about_us_section").slideToggle("fast");
    if ( state === true ) {
      include_about_us = true;
    } else {
      include_about_us = false;
    }

    buildable_check();
  });

  //
  $('input[id="include_sign_up"]').on('switchChange.bootstrapSwitch', function(event, state) {
    $("#sign_up_section").slideToggle("fast");
    if ( state === true ) {
      include_sign_up = true;
    } else {
      include_sign_up = false;

      buildable_check();
    }
  });

  //
  $('input[id="include_banners"]').on('switchChange.bootstrapSwitch', function(event, state) {
    $("#banners_section").slideToggle("fast");
    if ( state === true ) {
      include_banners = true;
    } else {
      include_banners = false;
    }

    buildable_check();
  });

  // Get the minimize event
  win.on('minimize', function() {
    // This is actually only an issue on OS X.
  });

  // Clear the selected banner.
  $(".clear-banner-btn").click(function(){
    var ndx = $(this).attr("data-index");

    $("#banner_img_" + ndx).val("");
    $("#banner_preview_" + ndx).html("");
    $("#banner_link_" + ndx).val("");
    $(this).hide();

    buildable_check();
  });

  // Build the list of background options.
  var bg_image_str = "";

  $.each(background_thumbs_list, function(key, val){
      bg_image_str += '<div class="unsplash-bg-imgs" style="float: left; margin: 2px; text-align: center; height: 100px; overflow: hidden; -webkit-border-radius: 10px; -moz-border-radius: 10px; border-radius: 10px; text-align: center;" data-img-index="' + key + '"><img src="' + val + '" style="height: 105px;" /></div>';
  });

  $("#bg_image_list").html(bg_image_str);

  $("body").on("hover",".unsplash-bg-imgs", function(){
    $(this).css("-webkit-box-shadow:inset", "0 0 7px 3px #919191");
    $(this).css("box-shadow:inset", "0 0 7px 3px #919191");
  });

  // Add class when mousing over bg image options.
  $("body").on("mouseover",".unsplash-bg-imgs", function(){
    $(this).addClass("bg-image-hover");
  });

  // Remove class when exiting bg image options
  $("body").on("mouseout",".unsplash-bg-imgs", function(){
    $(this).removeClass("bg-image-hover");
  });

  // Set unsplash bg image as selected.
  $("body").on("click", ".unsplash-bg-imgs", function(){
    var bg_image = background_image_list[$(this).attr("data-img-index")];

    // Get the bg base.
    var bg_base = path.parse(bg_image);

    background_img = "../images/" + bg_base.base;

    var bg_img = '<img style="height: 205px;" src="' + bg_image + '" />';
    $("#bg_preview").html(bg_img);

    buildable_check();
  });

  // replace the current background image with another.
  $("#bg_image_file").change(function(){
    $("#logo_file").val( $("#bg_image_file").val() );

    var bg_image = $("#bg_image_file").val();
    var base_img = "";

    // Get the background base.
    base_img = path.parse(bg_image);

    // Assign the global var.
    background_img = "../images/" + base_img.base;

    // Set up the bg image preview.
    var bg_img_styles = "";
    var img = new Image(bg_img);

    if ($(img).height > $(img).width) {
      bg_img_styles = "width: 215px;";
    } else {
      bg_img_styles = "height: 215px;";
    }

    img = null;

    var bg_img = '<img class="bg-preview" style="' + bg_img_styles + '" src="' + bg_image + '" />';

    $("#bg_preview").html(bg_img);

    buildable_check();
  });

  // Fire the banner #1 upload click event.
  $("#banner_upload_1").click(function(){
    $("#banner_img_1").click();
  });

  // Preview for banner image #1.
  $("#banner_img_1").change(function(){
    if ( $("#banner_img_1").val() !== null ) {
      var banner_image = $("#banner_img_1").val();
      var banner_img = '<img style="width: 500px;" src="' + banner_image + '" />';
      $("#banner_preview_1").html(banner_img);
      $("[data-index='1']").show();
    } else {
      $("#banner_preview_1").html("");
    }

    buildable_check();
  });

  // Fire the banner #2 upload click event.
  $("#banner_upload_2").click(function(){
    $("#banner_img_2").click();
  });

  // Preview for banner image #2.
  $("#banner_img_2").change(function(){
    if ( $("#banner_img_2").val() !== null ) {
      var banner_image = $("#banner_img_2").val();
      var banner_img = '<img style="width: 500px;" src="' + banner_image + '" />';
      $("#banner_preview_2").html(banner_img);
      $("[data-index='2']").show();
    } else {
      $("#banner_preview_2").html("");
    }

    buildable_check();
  });

  // Fire the banner #3 upload click event.
  $("#banner_upload_3").click(function(){
    $("#banner_img_3").click();
  });

  // Preview for banner image #3.
  $("#banner_img_3").change(function(){
    if ( $("#banner_img_3").val() !== null ) {
      var banner_image = $("#banner_img_3").val();
      var banner_img = '<img style="width: 500px;" src="' + banner_image + '" />';
      $("#banner_preview_3").html(banner_img);
      $("[data-index='3']").show();
    } else {
      $("#banner_preview_3").html("");
    }

    buildable_check();
  });

  // Leaderboard wikipedia.
  $("#leaderboard_wikipedia").click(function(){
    open("https://en.wikipedia.org/wiki/Web_banner");
  });

  // When a banner link is blurred, validate.
  $(".banner-links").blur(function(){
    if ( $(this).val() != "" ) {
      var link = $(this).val();
      var banner_link = $(this).attr("id").split("_");

      if(/^(http:\/\/|https:\/\/|http:\/\/|https:\/\/).[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/.test(link)){
          $("#warning_link_" + banner_link[2]).css("display", "none");
      } else {
          $("#warning_link_" + banner_link[2]).css("display", "block");
      }
    }

    buildable_check();
  });

  // Set up the wysiwyg editor.
  $("#about_us_html").summernote({
    height: 300,
    minHeight: 300,
    backColor: 'white'
  });

  // Configure the wysiwyg editor.
  $("#about_us_html").summernote();

  // On "App Name" blur, validate.
  $("#app_name").blur(function(){
    buildable_check();
  });

  // On "About Us" blur, validate.
  $("#about_us_html").blur(function(){
    buildable_check();
  });

  // On "Sign Up Code" blur, validate.
  $("#sign_up_code").blur(function(){
    buildable_check();
  });

  // Upload a background image.
  $("#bg_upload").click(function(){
    $("#bg_image_file").click();
  });

  // Upload the logo image.
  $(".logo-upload").click(function(){
    $("#bg_image_file").click();
  });

  // Upload a logo image.
  $("#logo_upload").click(function(){
    $("#logo_image_file").click();
  });

  // Show the apps folder.
  $("#show_built_apps").click(function(){
    gui.Shell.showItemInFolder( path.resolve('../release/') );
  });

  // Reset the builder app.
  $("#reset_application").click(function(){
    location.reload();
  })

  // Required fields blur event.
  $(".required").blur(function(){
    if ( $(this).val() !== null ) {
      buildable_check();
    }
  });

  // replace the current logo image with another.
  $("#logo_image_file").change(function(){
    var logo_image = $("#logo_image_file").val();
    var base_img = "";

    // Set the app variable for the logo.
    logo_img = logo_image;

    base_img = path.parse(logo_image);
    // if (process.platform == "win32" || process.platform == "win64") {
    //   base_img = logo_image.split('\\');
    // } else {
    //   base_img = logo_image.split('/');
    // }

    // Set up the logo image preview.
    var logo_img_styles = "";
    var img = new Image(logo_img);

    if ($(img).height > $(img).width) {
      logo_img_styles = "width: 130px;";
      $("#logo_preview").css("width", "120px");
    } else {
      logo_img_styles = "height: 130px;";
      $("#logo_preview").css("height", "120px");
      $("#logo_preview").css("max-width", "500px");
    }

    img = null;

    var logo_image = '<img style="' + logo_img_styles + '" src="' + logo_image + '" />';
    $("#logo_preview").html(logo_image);

    buildable_check();
  });

  // Build the app.
  $("#build_my_app_btn").click(function(){
    // Set up build, depending on whether we're on OS X or Win.
    if (process.platform === "darwin" && localStorage.nw_init !== "true") {
      // Test if Homebrew is installed.
      var has_brew = child_process.execSync('which brew');

      if ( String(has_brew) === "brew not found" ) {
        child_process.execSync('ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"', function (error, stdout, stderr) {
          if(error === null) {
            console.log(error.code);
          } else {
            console.log("Successfully installed Homebrew.");

            install_makensis();
            install_wine();
          }
        });
      } else {
        // Test for makensis and wine.
        var has_makensis = child_process.execSync('brew ls --versions makensis');
        if (has_makensis === "") {
            install_makensis();
        }

        var has_wine = child_process.execSync('brew ls --versions wine');

        if (has_wine === "") {
            install_wine();
        }
      }

      // Note in local storage that this has been run.
      localStorage.nw_init = true;
    }
    // End OS X setup.

    // // Set the application name.
    // nw.appName = title;

    // Clear out the build folder if it exists.
    remove_build_dir();

    // Show that the app is being built.
    $("#main_app_pnl").hide();
    $("#building_app_pnl").show();

    // Set variables.
    title = $("#app_title").val();
    about_us_html = $("#about_us_html").code();
    app_name = $("#app_name").val();
    sign_up_form_code = $("#sign_up_code").val();

    background_img = background_img || "../images/girl_at_dusk.jpg";
    logo_img = logo_img || "assets/images/logo.png";
    app_name = app_name || "Detergent";

    // Create the build folder and update as appropriate.
    ncp("source", "build", function(err){
      if (!err) {
          console.log("Successfully copied to 'build' directory.");

          // Change indicator.
          $(".busy-copying").removeClass("busy-active");
          $(".busy-downloading").addClass("busy-active");

          // Open the file for reading.
          fs.readFile("build/index.html", {encoding: 'utf-8'}, function(err,data){
            // If there's no error, do this stuff.
            if (!err) {
              var user_data = data.replace(/{{title}}/g, title);
              user_data = user_data.replace(/{{app_name}}/g, app_name);
              user_data = user_data.replace(/{{logo_img}}/g, logo_img);
              //user_data = user_data.replace(/{{logo_tagline}}/g, tag);
              if ( include_about_us == true ) {
                user_data = user_data.replace(/{{about_us}}/g, about_us_html);
              } else {
                user_data = $.strRemove("#about_us_nav", user_data);
              }

              // Build banner html if the switch is set to true.
              if ( include_banners == true ) {
                user_data = user_data.replace(/{{banners}}/g, add_banner_items());
              } else {
                user_data = $.strRemove("#banner_section", user_data);
              }

              // Check for and replace double-slash URLs for sign-up code.
              if ( include_sign_up === true ) {
                sign_up_form_code = sign_up_form_code.replace(/\"\/\//g, '"https://');
                sign_up_form_code = sign_up_form_code.replace(/\'\/\//g, "'https://");

                user_data = user_data.replace(/{{sign_up_form}}/g, sign_up_form_code);
              } else {
                user_data = $.strRemove("#sign_up_pnl", user_data);
                //user_data = user_data.replace(/{{sign_up_form}}/g, "");
              }

              fs.writeFile('build/index.html', user_data, 'utf-8', function (err) {
                if (err) throw err;
                console.log('Index.html has been built successfully.');
              });

              $("#file_editor").val(user_data);
            } else {
              // There is an error, so do this:
              console.log(err);
              $("#file_editor").val("There is an error.");
            }
          });

          // Open the file for reading.
          fs.readFile("build/assets/stylesheets/main.css", {encoding: 'utf-8'}, function(err,data){
            // If there's no error, do this stuff.
            if (!err) {
              console.log('received data: ' + data);
              var user_data = data.replace("{{background_image}}", background_img);

              fs.writeFile('build/assets/stylesheets/main.css', user_data, 'utf-8', function (err) {
                if (err) throw err;
                console.log('Main.css compiling complete.');
              });
            } else {
              console.log("Error: " + err);
            }
          });

          // Open the file for reading.
          fs.readFile("build/package.json", {encoding: 'utf-8'}, function(err,data){
            debugger;
            // If there's no error, do this stuff.
            if (!err) {
              title = title.replace(/\W+/g, " ");
              console.log("Title: " + title);
              var user_data = data.replace(/{{app_title}}/g, title);

              fs.writeFile('build/package.json', user_data, 'utf-8', function (err) {
                if (err) throw err;
                console.log('Package.json updated.');
              });
            } else {
              console.log("Error: " + err);
            }
          });

          // Move the images to the 'build' folder.
          if ( background_img !== "../images/girl_at_dusk.jpg" && logo_img !== "assets/images/logo.png" ) {
            $.when( inject_logo_image() ).then( inject_background_image() );
          } else if ( background_img !== "../images/girl_at_dusk.jpg" && logo_img === "assets/images/logo.png" ) {
            inject_background_image();
          } else if ( background_img === "../images/girl_at_dusk.jpg" && logo_img !== "assets/images/logo.png" ) {
            inject_logo_image();
          } else if ( background_img === "../images/girl_at_dusk.jpg" && logo_img === "assets/images/logo.png" ) {
            console.log("No images added.");
          }
      } else {
        return console.error(err);
      }
    });
  });

  // Push console.log feedback to the progress div while building.
  if (typeof console  != "undefined")
    if (typeof console.log != 'undefined')
        console.olog = console.log;
    else
        console.olog = function() {};

    console.log = function(message) {
        console.olog(message);
        $('#build_div').html('<p>' + message + '</p>');
    };
    console.error = console.debug = console.info =  console.log
});
