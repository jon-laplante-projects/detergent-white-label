# define installer name
OutFile "Detergent Installer.exe"

# set desktop as install directory
InstallDir $PROGRAMFILES32
# InstallDir $DESKTOP

# default section start
Section

# define output path
SetOutPath $INSTDIR

# specify file to go in output path
File test.txt

# define uninstaller name
WriteUninstaller $INSTDIR\Detergent Uninstaller.exe


#-------
# default section end
SectionEnd

# create a section to define what the uninstaller does.
# the section will always be named "Uninstall"
Section "Uninstall"

# Always delete uninstaller first
Delete $INSTDIR\Detergent Uninstaller.exe

# now delete installed file
Delete $INSTDIR\test.txt

SectionEnd
